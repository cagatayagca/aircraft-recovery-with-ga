package edu.ozu.aircraftrecoveryga;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import edu.ozu.model.AircraftChromosome;
import edu.ozu.util.TruthTableUtil;

@SpringBootApplication
public class AircraftRecoveryGaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AircraftRecoveryGaApplication.class, args);

		List<Integer> tester = new ArrayList<>(Arrays.asList(0, 1, 2));
		List<AircraftChromosome> solutionTest = TruthTableUtil.generateInitPopulation(5, tester);
		System.out.println(solutionTest);
	}

}
