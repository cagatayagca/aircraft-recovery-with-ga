package edu.ozu.util;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ozu.enums.ActionType;
import edu.ozu.enums.DelayType;
import edu.ozu.model.AircraftChromosome;
import edu.ozu.model.AircraftGene;

public class TruthTableUtil {
	private TruthTableUtil() {
	}

	private static int tableSize = 500;
	private static final Logger logger = LoggerFactory.getLogger(TruthTableUtil.class);

	public static List<AircraftGene> generateTruthTable(final List<Integer> disruptedAicraftIds) {
		return generateRandomSolutionList(tableSize, disruptedAicraftIds);
	}

	public static List<AircraftChromosome> generateInitPopulation(final int initPopulationSize,
			final List<Integer> disruptedAicraftIds) {
		ArrayList<AircraftChromosome> population = new ArrayList<>();
		AircraftChromosome individual;
		ArrayList<AircraftGene> newGenes;
		boolean loop = true;
		while (loop) {
			individual = new AircraftChromosome();
			newGenes = new ArrayList<>();
			for (Integer disruptedAircraft : disruptedAicraftIds) {
				newGenes.add(generateRandomGeneWithId(disruptedAircraft));
			}
			individual.setAircraftGenes(newGenes);
			population.add(individual);
			if (population.size() >= initPopulationSize) {
				loop = false;
			}
		}
		return population;
	}

	private static List<AircraftGene> generateRandomSolutionList(final int listSize,
			final List<Integer> disruptedAicraftIdList) {
		ArrayList<AircraftGene> table = new ArrayList<>();
		boolean loop = true;
		try {
			while (loop) {
				for (int aircraftId : disruptedAicraftIdList) {
					table.add(generateRandomGeneWithId(aircraftId));
				}
				if (table.size() >= listSize)
					loop = false;
			}
		} catch (Exception e) {
			logger.error("Error while generateRandomSolutionList: {}", e.getMessage());
		}
		return table;
	}

	private static AircraftGene generateRandomGeneWithId(final int aircraftId) {
		AircraftGene gene = new AircraftGene();
		ActionType action = ActionType.getRandomAction();
		gene.setActionType(action);
		if (action != ActionType.DELAY) {
			gene.setDelay(DelayType.NONE);
		} else {
			gene.setDelay(DelayType.getRandomDelay(DelayType.NONE));
		}
		gene.setAircraftId(aircraftId);

		return gene;
	}
}
