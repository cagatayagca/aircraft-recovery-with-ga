package edu.ozu.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum DelayType {

	NONE(0), ONE(30), TWO(60), THREE(90), FOUR(120), FIVE(150);

	private int delayInMinutes;

	private DelayType(final int delayValue) {
		this.delayInMinutes = delayValue;
	}

	private static final List<DelayType> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static final Random RANDOM = new Random();

	public static DelayType getRandomDelay(DelayType delayToExclude) {
		boolean loop = true;
		DelayType delay = null;
		if (delayToExclude != null) {
			while (loop) {
				delay = VALUES.get(RANDOM.nextInt(SIZE));
				if (delay != delayToExclude) {
					loop = false;
				}
			}
		} else {
			delay = VALUES.get(RANDOM.nextInt(SIZE));
		}

		return delay;
	}

	public int getDelayInMinutes() {
		return delayInMinutes;
	}

}
