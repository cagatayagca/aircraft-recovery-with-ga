package edu.ozu.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum ActionType {

	DELAY, CANCEL, EXCHANGE;

	private static final List<ActionType> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = VALUES.size();
	private static final Random RANDOM = new Random();

	public static ActionType getRandomAction() {
		return VALUES.get(RANDOM.nextInt(SIZE));
	}

}
