package edu.ozu.model;

import java.math.BigDecimal;
import java.util.List;

public class AircraftChromosome {

	private List<AircraftGene> aircraftGenes;

	private BigDecimal fitnessValue;

	public AircraftChromosome() {
	}

	public AircraftChromosome(final List<AircraftGene> aircraftGenes, final BigDecimal fitnessValue) {
		super();
		this.aircraftGenes = aircraftGenes;
		this.fitnessValue = fitnessValue;
	}

	public List<AircraftGene> getAircraftGenes() {
		return aircraftGenes;
	}

	public void setAircraftGenes(List<AircraftGene> aircraftGenes) {
		this.aircraftGenes = aircraftGenes;
	}

	public BigDecimal getFitnessValue() {
		return fitnessValue;
	}

	public void setFitnessValue(BigDecimal fitnessValue) {
		this.fitnessValue = fitnessValue;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder("|").append("fitness:")
				.append(this.fitnessValue == null ? "0" : this.fitnessValue.toString()).append("|genes:")
				.append(this.aircraftGenes).append("|");
		return stringBuilder.toString();
	}

}
