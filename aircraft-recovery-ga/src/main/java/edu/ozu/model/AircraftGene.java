package edu.ozu.model;

import edu.ozu.enums.ActionType;
import edu.ozu.enums.DelayType;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class AircraftGene {

	private ActionType actionType;
	private DelayType delay;
	private int aircraftId;

	public AircraftGene(final ActionType action, final DelayType delay, final int id) {
		this.actionType = action;
		this.delay = delay;
		this.aircraftId = id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("|action:").append(this.actionType.name()).append("|delay:")
				.append(this.delay.name()).append("|AircraftID:").append(this.aircraftId).append("|");
		return builder.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (obj instanceof AircraftGene) {
			AircraftGene solution = (AircraftGene) obj;
			if (this.actionType == solution.getActionType() && this.delay == solution.getDelay()
					&& this.getAircraftId() == solution.aircraftId)
				return true;
			return false;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int) aircraftId * actionType.hashCode() * delay.hashCode();
	}

	public ActionType getActionType() {
		return actionType;
	}

	public void setActionType(ActionType actionType) {
		this.actionType = actionType;
	}

	public DelayType getDelay() {
		return delay;
	}

	public void setDelay(DelayType delay) {
		this.delay = delay;
	}

	public int getAircraftId() {
		return aircraftId;
	}

	public void setAircraftId(int aircraftId) {
		this.aircraftId = aircraftId;
	}

}
